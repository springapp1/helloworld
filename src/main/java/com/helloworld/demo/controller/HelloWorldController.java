package com.helloworld.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class HelloWorldController {
 
    @GetMapping("/hello")
    public String sayHello(@RequestParam int count) {
    	int i=count;
    	i++;
    	System.out.println("request count: " +i);
    	return "Hello World!";
    }
}