package com.helloworld.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.helloworld.demo.controller.HelloWorldController;

public class HelloworldTest {
	
	private HelloWorldController helloworldcontroller;
	
	@BeforeEach                                         
    public void setUp() throws Exception {
        helloworldcontroller = new HelloWorldController();
	}  
	@Test                                               
    @DisplayName("Say Hello")   
    public void testhello() {
        assertEquals("Hello World!", helloworldcontroller.sayHello(1), "1");          
    }   

}
